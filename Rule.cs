﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShell
{
    public class Rule
    {
        BindingList<Fact> conditions = new BindingList<Fact>();
        BindingList<Fact> results = new BindingList<Fact>();
        public string Name { get; set; }
        public string Description { get; set; }
        public BindingList<Fact> Conditions { 
            get { return conditions; } 
            set { 
                conditions = value;
                Description = ToString();
            } 
        }
        public BindingList<Fact> Results {
            get { return results; }
            set
            {
                results = value;
                Description = ToString();
            }
        }
        public string Reason { get; set; }
        public Rule(string name, BindingList<Fact> conditions, BindingList<Fact> results, string reason)
        {
            Name = name;
            Conditions = conditions;
            Results = results;
            Reason = reason;
        }
        
        public Rule() {}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ЕСЛИ ");
            if (Conditions.Count == 0)
                sb.Append("(пусто) ");
            else
            {
                for (int i = 0; i < Conditions.Count; i++)
                {
                    sb.Append(Conditions[i].ToString());
                    sb.Append(" ");
                    if (i != Conditions.Count - 1)
                        sb.Append("И ");
                }
            }
            sb.Append("\nТО ");
            if (Results.Count == 0)
                sb.Append("(пусто) ");
            else
            {
                for (int i = 0; i < Results.Count; i++)
                {
                    sb.Append(Results[i].ToString());
                    sb.Append(" ");
                    if (i != Results.Count - 1)
                        sb.Append("И ");
                }
            }
            return sb.ToString();
        }
    }
}
