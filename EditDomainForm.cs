﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace MyShell
{
    public partial class EditDomainForm : Form
    {
        Domain domain;
        BindingList<DomainValue> newDomainValueCollection;

        public EditDomainForm(Domain domain)
        {
            InitializeComponent();
            okBtn.DialogResult = DialogResult.OK;
            okBtn.Enabled = OpenBtnOk();

            newDomainValueCollection = new BindingList<DomainValue>();
            this.domain = domain;

            domainNameTxt.Text = domain.Name;
            foreach (var v in domain.Values)
                newDomainValueCollection.Add(v);

            domainValueList.DataSource = newDomainValueCollection;
            domainValueList.DisplayMember = "Value";
        }

        private void addDomainValueBtn_Click(object sender, EventArgs e)
        {
            DomainValue newDomainValue = new DomainValue();
            EditDomainValueForm edvf = new EditDomainValueForm(newDomainValue);
            if (edvf.ShowDialog() == DialogResult.OK)
                if ((newDomainValueCollection.AsQueryable()).Contains<DomainValue>(newDomainValue, new Comparers.DomainValueComparer()))
                    MessageBox.Show("Такое доменное значение уже существует");
                else
                    newDomainValueCollection.Add(newDomainValue);
        }


        private void delDomainValueBtn_Click(object sender, EventArgs e)
        {
            int index = domainValueList.SelectedIndex;
            newDomainValueCollection.RemoveAt(index);
        }

        private void editDomainValueBtn_Click(object sender, EventArgs e)
        {
            int index = domainValueList.SelectedIndex;
            DomainValue editedDomainValue = newDomainValueCollection[index];
            EditDomainValueForm edvf = new EditDomainValueForm(editedDomainValue);
            if (edvf.ShowDialog() == DialogResult.OK)
                if (newDomainValueCollection.Contains(editedDomainValue))
                    MessageBox.Show("Такое доменное значение уже существует");
                else
                    newDomainValueCollection[index] = editedDomainValue;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            domain.Name = domainNameTxt.Text;
            domain.Values = newDomainValueCollection;
        }

        private void domainNameTxt_TextChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenBtnOk();
        }

        bool OpenBtnOk()
        {
            return !string.IsNullOrEmpty(domainNameTxt.Text);
        }
    }
}
