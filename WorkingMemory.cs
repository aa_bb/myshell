﻿using System.Collections.Generic;

namespace MyShell
{
    public class WorkingMemory
    {
        public Variable GoalVariable { get; set; }

        public List<Fact> TrueFacts { get; set; }

        public List<Rule> UntriggeredRules { get; set; }

        public List<Rule> TriggeredRules { get; }

        //public Stack<Rule> RulesStack { get; }

        public Dictionary<Variable, Rule> InferenceDictionary { get; }

        public WorkingMemory()
        {
            TrueFacts = new List<Fact>();
            UntriggeredRules = new List<Rule>();
            TriggeredRules = new List<Rule>();
            //RulesStack = new Stack<Rule>();
            InferenceDictionary = new Dictionary<Variable, Rule>();
        }
    }
}
