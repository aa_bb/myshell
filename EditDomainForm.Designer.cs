﻿namespace MyShell
{
    partial class EditDomainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.domainNameTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.domainValueList = new System.Windows.Forms.ListBox();
            this.addDomainValueBtn = new System.Windows.Forms.Button();
            this.editDomainValueBtn = new System.Windows.Forms.Button();
            this.delDomainValueBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название домена:";
            // 
            // domainNameTxt
            // 
            this.domainNameTxt.Location = new System.Drawing.Point(20, 39);
            this.domainNameTxt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.domainNameTxt.Name = "domainNameTxt";
            this.domainNameTxt.Size = new System.Drawing.Size(420, 22);
            this.domainNameTxt.TabIndex = 1;
            this.domainNameTxt.TextChanged += new System.EventHandler(this.domainNameTxt_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Значения домена:";
            // 
            // domainValueList
            // 
            this.domainValueList.FormattingEnabled = true;
            this.domainValueList.ItemHeight = 16;
            this.domainValueList.Location = new System.Drawing.Point(20, 94);
            this.domainValueList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.domainValueList.Name = "domainValueList";
            this.domainValueList.Size = new System.Drawing.Size(420, 244);
            this.domainValueList.TabIndex = 3;
            // 
            // addDomainValueBtn
            // 
            this.addDomainValueBtn.Location = new System.Drawing.Point(23, 352);
            this.addDomainValueBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addDomainValueBtn.Name = "addDomainValueBtn";
            this.addDomainValueBtn.Size = new System.Drawing.Size(109, 30);
            this.addDomainValueBtn.TabIndex = 4;
            this.addDomainValueBtn.Text = "Добавить";
            this.addDomainValueBtn.UseVisualStyleBackColor = true;
            this.addDomainValueBtn.Click += new System.EventHandler(this.addDomainValueBtn_Click);
            // 
            // editDomainValueBtn
            // 
            this.editDomainValueBtn.Location = new System.Drawing.Point(178, 352);
            this.editDomainValueBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editDomainValueBtn.Name = "editDomainValueBtn";
            this.editDomainValueBtn.Size = new System.Drawing.Size(109, 30);
            this.editDomainValueBtn.TabIndex = 5;
            this.editDomainValueBtn.Text = "Изменить";
            this.editDomainValueBtn.UseVisualStyleBackColor = true;
            this.editDomainValueBtn.Click += new System.EventHandler(this.editDomainValueBtn_Click);
            // 
            // delDomainValueBtn
            // 
            this.delDomainValueBtn.Location = new System.Drawing.Point(331, 352);
            this.delDomainValueBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delDomainValueBtn.Name = "delDomainValueBtn";
            this.delDomainValueBtn.Size = new System.Drawing.Size(109, 30);
            this.delDomainValueBtn.TabIndex = 6;
            this.delDomainValueBtn.Text = "Удалить";
            this.delDomainValueBtn.UseVisualStyleBackColor = true;
            this.delDomainValueBtn.Click += new System.EventHandler(this.delDomainValueBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(331, 394);
            this.okBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(108, 28);
            this.okBtn.TabIndex = 7;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // EditDomainForm
            // 
            this.AcceptButton = this.okBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 432);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.delDomainValueBtn);
            this.Controls.Add(this.editDomainValueBtn);
            this.Controls.Add(this.addDomainValueBtn);
            this.Controls.Add(this.domainValueList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.domainNameTxt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditDomainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование домена";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox domainNameTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox domainValueList;
        private System.Windows.Forms.Button addDomainValueBtn;
        private System.Windows.Forms.Button editDomainValueBtn;
        private System.Windows.Forms.Button delDomainValueBtn;
        private System.Windows.Forms.Button okBtn;
    }
}