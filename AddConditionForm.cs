﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShell
{
    public partial class AddConditionForm : Form
    {
        Fact fact;
        BindingList<Variable> variablesList;
        BindingList<Domain> domainList;

        public AddConditionForm(Fact fact, BindingList<Variable> variablesList, BindingList<Domain> domainList, bool isResult)
        {
            InitializeComponent();
            this.fact = fact;
            this.variablesList = variablesList;
            this.domainList = domainList;

            okBtn.Enabled = OpenOkBtn();
            BindingList<Variable> tmp;
            if (isResult) {
                tmp = new BindingList<Variable>();
                foreach (var v in variablesList)
                if (v.VariableType != VariableType.Asked)
                    tmp.Add(v);
        }
            else tmp = variablesList;
            foreach (Variable v in tmp)
            {
                if (v.Domain.Values.Count != 0)
                    conditionLeftCbx.Items.Add(v);
                if (v.Equals(fact.Variable))
                {
                    conditionLeftCbx.SelectedItem = v;
                    FillDomainCbx();
                }
            }
            if (conditionLeftCbx.SelectedIndex == -1)
                conditionRightCbx.Enabled = false;
        }

        void FillDomainCbx()
        {
            conditionRightCbx.Items.Clear();
            Domain domain = fact.Variable.Domain;
            foreach (var d in domain.Values)
            {
                conditionRightCbx.Items.Add(d);
                if (fact.Value != null && d.Value.Equals(fact.Value.Value))
                    conditionRightCbx.SelectedItem = d;
            }
        }

        private void conditionLeftCbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (conditionLeftCbx.SelectedItem == null)
                return;
            fact.Variable = (Variable)conditionLeftCbx.SelectedItem;
            okBtn.Enabled = OpenOkBtn();
            conditionRightCbx.Enabled = true;
            FillDomainCbx();
        }

        private void conditionRightCbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            fact.Value = (DomainValue)conditionRightCbx.SelectedItem;
            okBtn.Enabled = OpenOkBtn();
        }

        bool OpenOkBtn()
        {
            return conditionLeftCbx.SelectedIndex != -1 && conditionRightCbx.SelectedIndex != -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Variable newVariable = new Variable();
            CreateVariableForm cvf = new CreateVariableForm(newVariable, domainList);
            if (cvf.ShowDialog() == DialogResult.OK)
                variablesList.Add(newVariable);
            conditionLeftCbx.Items.Add(newVariable);
            conditionLeftCbx.SelectedItem = newVariable;
        }
    }
}
