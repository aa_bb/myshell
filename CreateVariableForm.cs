﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MyShell
{
    public partial class CreateVariableForm : Form
    {
        Variable variable;
        VariableType varType;
        BindingList<Domain> domainList;
        public CreateVariableForm(Variable variable, BindingList<Domain> domainList)
        {
            InitializeComponent();
            okBtn.DialogResult = DialogResult.OK;
            okBtn.Enabled = OpenOkBtn();

            this.variable = variable;
            varType = variable.VariableType;
            this.domainList = domainList;

            varNameTxt.Text = variable.Name;
            label3.Visible = !radioConcluded.Checked;
            txtQuestion.Visible = !radioConcluded.Checked;

            foreach (var d in domainList)
            {
                domainCbx.Items.Add(d);
                if (d.Equals(variable.Domain))
                    domainCbx.SelectedItem = d;
            }

            switch (variable.VariableType)
            {
                case VariableType.Asked:
                    radioAsked.Checked = true;
                    break;
                case VariableType.Concluded:
                    radioConcluded.Checked = true;
                    break;
                case VariableType.Conclude_Asked:
                    radioAskedConcluded.Checked = true;
                    break;
            }
            txtQuestion.Text = variable.QuestionText;
        }

        private void radioAsked_CheckedChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
            if (radioAsked.Checked)
                varType = VariableType.Asked;   
        }

        private void radioConcluded_CheckedChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
            label3.Visible = !radioConcluded.Checked;
            txtQuestion.Visible = !radioConcluded.Checked;
            if (radioConcluded.Checked) 
                varType = VariableType.Concluded;
        }

        private void radioAskedConcluded_CheckedChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
            if (radioAskedConcluded.Checked)
                varType = VariableType.Conclude_Asked;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            variable.Name = varNameTxt.Text;
            variable.VariableType = varType;
            variable.Domain = (Domain) domainCbx.SelectedItem;
            variable.QuestionText = txtQuestion.Text;
        }

        private void varNameTxt_TextChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
            if (string.IsNullOrEmpty(variable.QuestionText))
                txtQuestion.Text = varNameTxt.Text + "?";
        }

        private void txtQuestion_TextChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
        }

        private bool OpenOkBtn()
        {
            bool name = !string.IsNullOrEmpty(varNameTxt.Text);
            bool question = radioConcluded.Checked && string.IsNullOrEmpty(txtQuestion.Text) || !string.IsNullOrEmpty(txtQuestion.Text);
            bool domain = domainCbx.SelectedItem != null;
            return name && domain && question;
        }

        private void domainCbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
        }

        private void btnAddDomain_Click(object sender, EventArgs e)
        {
            var domain = new Domain();
            EditDomainForm edf = new EditDomainForm(domain);
            if (edf.ShowDialog() == DialogResult.OK)
                domainList.Add(domain);
            domainCbx.Items.Add(domain);
            domainCbx.SelectedItem = domain;
        }
    }
}
