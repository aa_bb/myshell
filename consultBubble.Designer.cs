﻿namespace MyShell
{
    partial class ConsultBubble
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bubbleLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bubbleLabel
            // 
            this.bubbleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bubbleLabel.Location = new System.Drawing.Point(11, 10);
            this.bubbleLabel.Name = "bubbleLabel";
            this.bubbleLabel.Size = new System.Drawing.Size(284, 76);
            this.bubbleLabel.TabIndex = 0;
            this.bubbleLabel.Text = "sample text";
            // 
            // ConsultBubble
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.bubbleLabel);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ConsultBubble";
            this.Size = new System.Drawing.Size(308, 86);
            this.Resize += new System.EventHandler(this.ConsultBubble_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label bubbleLabel;
    }
}
