﻿using System.ComponentModel;

namespace MyShell
{
    public enum VariableType
    {
        [Description("Выводимая")]
        Concluded,

        [Description("Запрашиваемая")]
        Asked,

        [Description("Выводимо-запрашиваемая")]
        Conclude_Asked
    }
}
