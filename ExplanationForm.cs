﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MyShell
{
    public partial class ExplanationForm : Form
    {
        WorkingMemory memory;
        bool isExpanded;
        public ExplanationForm(WorkingMemory memory)
        {
            InitializeComponent();
            this.memory = memory;
            isExpanded = false;
            linkLabel1.Text = isExpanded ? "Свернуть все" : "Развернуть все";
            foreach (var fact in memory.TrueFacts)
                dataGridView1.Rows.Add(fact.Variable.ToString(), fact.Value.Value);
            InitializeTreeView();
        }

        private void InitializeTreeView()
        {
            this.InitializeTreeViewItem(memory.GoalVariable);
        }

        private void InitializeTreeViewItem(Variable variable, TreeNode parent = null)
        {
            var str1 = "ЦЕЛЬ: " + variable.Name;
            var fact1 = memory.TrueFacts.FirstOrDefault(f => f.Variable.Equals(variable));
            var str2 = fact1 == null ? str1 + "не удалось получить" : str1 + " = " + fact1.Value.Value;
            if (variable.VariableType == VariableType.Asked)
                str2 += " [введена пользователем]";

            var treeViewItem1 = new TreeNode(str2);

            var parent1 = treeViewItem1;
            if (parent != null)
                parent.Nodes.Add(parent1);
            else
                treeView1.Nodes.Add(parent1);

            if (!memory.InferenceDictionary.ContainsKey(variable))
                return;

            var inference = memory.InferenceDictionary[variable];
            var items = parent1.Nodes;
            var treeViewItem2 = new TreeNode(inference.ToString());
            items.Add(treeViewItem2);
            foreach (var fact2 in inference.Conditions)
                InitializeTreeViewItem(fact2.Variable, parent1);
        }

        private void AllExpand(object temp, bool expand)
        {
            if (temp is TreeNode item)
            {
                if (expand)
                    item.Expand();
                else
                    item.Collapse();
                foreach (var subitem in item.Nodes)
                    AllExpand(subitem, expand);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.Text = isExpanded ? "Свернуть все" : "Развернуть все";

            foreach (var item in treeView1.Nodes)
                AllExpand(item, !isExpanded);

            this.isExpanded = !this.isExpanded;
        }
        
    }
}
