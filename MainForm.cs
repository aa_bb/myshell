﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Linq;

namespace MyShell
{
    public partial class MainForm : Form
    {
        BindingList<Domain> domainList;
        BindingList<Variable> variableList;
        BindingList<Rule> ruleList;
        string fileName = null;
        public MainForm()
        {
            // init
            InitializeComponent();
            domainList = new BindingList<Domain>();
            variableList = new BindingList<Variable>();
            ruleList = new BindingList<Rule>();
            // init

            // variables
            dgvVars.DataSource = variableList;
            dgvVars.Columns[0].HeaderText = "Имя";
            dgvVars.Columns[1].HeaderText = "Домен";
            dgvVars.Columns[3].HeaderText = "Тип переменной";
            dgvVars.Columns[2].Visible = false;
            dgvVars.Columns[4].Visible = false;
            dgvVars.Columns[5].Visible = false;
            foreach (DataGridViewColumn c in dgvVars.Columns)
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            // variables

            // domains
            dgvDomain.DataSource = domainList;
            dgvDomain.Columns[0].HeaderText = "Имя";
            dgvDomain.Columns[1].Visible = false;
            dgvDomain.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            domainValueList.DisplayMember = "Value";
            listBox1.DisplayMember = "Value";
            // domains

            //rules
            dgvRules.DataSource = ruleList;
            dgvRules.Columns[0].HeaderText = "Имя";
            dgvRules.Columns[1].HeaderText = "Описание";
            dgvRules.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvRules.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            for (int i = 2; i < dgvRules.Columns.Count; i++)
                dgvRules.Columns[i].Visible = false;
            //rules
        }

        private void addDomainBtn_Click(object sender, EventArgs e)
        {
            Domain newDomain = new Domain();
            EditDomainForm edf = new EditDomainForm(newDomain);
            if (edf.ShowDialog() == DialogResult.OK)
                domainList.Add(newDomain);
        }

        private void editDomainBtn_Click(object sender, EventArgs e)
        {
            if (dgvDomain.CurrentCell == null)
                return;
            int index = dgvDomain.CurrentCell.RowIndex;
            Domain editedDomain = domainList[index];
            if (editedDomain.UsedCount != 0)
            {
                MessageBox.Show("Вы не можете изменить этот домен, т.к. он используется в " + editedDomain.UsedCount + " правилах");
                return;
            }
            EditDomainForm edf = new EditDomainForm(editedDomain);
            if (edf.ShowDialog() == DialogResult.OK)
            {
                domainList[index] = editedDomain;
                domainValueList.DataSource = domainList[index].Values;
            }
        }

        private void delDomainBtn_Click(object sender, EventArgs e)
        {
            var selected = dgvDomain.SelectedRows;
            for (int i = selected.Count - 1; i >= 0; i--)
            {
                int index = selected[i].Index;
                if (domainList[index].UsedCount != 0)
                    MessageBox.Show("Вы не можете удалить этот домен, т.к. он используется в " + domainList[index].UsedCount + " правилах");
                else
                    domainList.RemoveAt(index);
            }
        }

        private void открытьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
            if (ofd.FileName != "")
            {
                FileStream fs = (FileStream)ofd.OpenFile();
                StreamReader sr = new StreamReader(fs);
                string domainJson = sr.ReadLine();
                string variableJson = sr.ReadLine();
                string ruleJson = sr.ReadLine();

                try
                {
                    List<Domain> jsonDomainList = JsonConvert.DeserializeObject<List<Domain>>(domainJson);
                    List<Variable> jsonVariableList = JsonConvert.DeserializeObject<List<Variable>>(variableJson);
                    List<Rule> jsonRuleList = JsonConvert.DeserializeObject<List<Rule>>(ruleJson);

                    ruleList = new BindingList<Rule>(jsonRuleList);
                    variableList = new BindingList<Variable>(jsonVariableList);
                    domainList = new BindingList<Domain>(jsonDomainList);
                    foreach (Rule rule in ruleList)
                    {
                        foreach (Fact f in rule.Conditions)
                        {
                            int usedCount = f.Variable.UsedCount;
                            f.Variable = jsonVariableList.Where(item => f.Variable.Equals(item)).First();
                            f.Variable.UsedCount = usedCount;
                        }
                        foreach (Fact f in rule.Results)
                        {
                            int usedCount = f.Variable.UsedCount;
                            f.Variable = jsonVariableList.Where(item => f.Variable.Equals(item)).First();
                            f.Variable.UsedCount = usedCount;
                        }
                    }

                    foreach (Variable variable in variableList)
                    {
                        int usedCount = variable.UsedCount;
                        variable.Domain = jsonDomainList.Where(item => variable.Domain.Equals(item)).First();
                        variable.Domain.UsedCount += usedCount;
                    }
                    dgvRules.DataSource = ruleList;
                    dgvDomain.DataSource = domainList;
                    dgvVars.DataSource = variableList;
                    fileName = ofd.FileName;
                }
                catch (Exception)
                {
                    MessageBox.Show("Файл некорректного формата или поврежден");
                }
                sr.Close();
                fs.Close();
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveChose();
        }

        bool SaveChose()
        {
            if (fileName == null)
                return SaveAs();
            return Save();
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        bool Save()
        {
            File.WriteAllText(fileName, string.Empty);
            FileStream fs = File.OpenWrite(fileName);
            StreamWriter sw = new StreamWriter(fs);

            string domainJson = JsonConvert.SerializeObject(domainList);
            string variableJson = JsonConvert.SerializeObject(variableList);
            string ruleJson = JsonConvert.SerializeObject(ruleList);

            sw.WriteLine(domainJson);
            sw.WriteLine(variableJson);
            sw.WriteLine(ruleJson);

            sw.Close();
            fs.Close();
            return true;
        }
        bool SaveAs()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.ShowDialog();
            if (sfd.FileName != "")
            {
                FileStream fs = (FileStream)sfd.OpenFile();
                StreamWriter sw = new StreamWriter(fs);
                string domainJson = JsonConvert.SerializeObject(domainList);
                string variableJson = JsonConvert.SerializeObject(variableList);
                string ruleJson = JsonConvert.SerializeObject(ruleList);

                sw.WriteLine(domainJson);
                sw.WriteLine(variableJson);
                sw.WriteLine(ruleJson);

                fileName = sfd.FileName;
                sw.Close();
                fs.Close();
                return true;
            }
            return false;
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDomain.CurrentCell == null)
                return;

            var domainIdx = dgvDomain.CurrentCell.RowIndex;
            domainValueList.DataSource = domainList[domainIdx].Values;
        }

        private void dataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVars.CurrentCell == null)
            {
                txtQuestion.Text = "";
                return;
            }

            var variableIdx = dgvVars.CurrentCell.RowIndex;
            txtQuestion.Text = variableList[variableIdx].QuestionText;
            listBox1.DataSource = variableList[variableIdx].Domain.Values;
        }

        private void addVarBtn_Click(object sender, EventArgs e)
        {
            var newVariable = new Variable();
            var cvf = new CreateVariableForm(newVariable, domainList);
            if (cvf.ShowDialog() == DialogResult.OK)
                variableList.Add(newVariable);
        }

        private void editVarBtn_Click(object sender, EventArgs e)
        {
            if (dgvVars.CurrentCell == null)
                return;
            int index = dgvVars.CurrentCell.RowIndex;
            var editedVariable = variableList[index];
            if (editedVariable.UsedCount != 0)
            {
                MessageBox.Show("Вы не можете изменить эту переменную, т.к. она используется в " + editedVariable.UsedCount + " правилах");
                return;
            }
            var cvf = new CreateVariableForm(editedVariable, domainList);
            if (cvf.ShowDialog() == DialogResult.OK)
            {
                variableList[index] = editedVariable;
                txtQuestion.Text = variableList[index].QuestionText;
            }
        }

        private void delVarBtn_Click(object sender, EventArgs e)
        {
            var selected = dgvVars.SelectedRows;
            for (int i = selected.Count - 1; i >= 0; i--)
            {
                int index = selected[i].Index;
                if (variableList[index].UsedCount != 0)
                    MessageBox.Show("Вы не можете удалить эту переменную, т.к. она используется в " + variableList[index].UsedCount + " правилах");
                else
                    variableList.RemoveAt(index);
            }
        }

        private void addRuleBtn_Click(object sender, EventArgs e)
        {
            Rule newRule = new Rule();
            EditRuleForm erf = new EditRuleForm(newRule, variableList, domainList);
            if (erf.ShowDialog() == DialogResult.OK)
            {
                int index = dgvRules.CurrentRow.Index;
                if (index == -1 || index == ruleList.Count - 1)
                    ruleList.Add(newRule);
                else
                    ruleList.Insert(index + 1, newRule);
            }
        }

        private void editRuleBtn_Click(object sender, EventArgs e)
        {
            if (dgvRules.CurrentCell == null)
                return;
            int index = dgvRules.CurrentCell.RowIndex;
            Rule editedRule = ruleList[index];
            EditRuleForm erf = new EditRuleForm(editedRule, variableList, domainList);
            if (erf.ShowDialog() == DialogResult.OK)
            {
                ruleList[index] = editedRule;
                conditionsList.DataSource = ruleList[index].Conditions;
                resultsList.DataSource = ruleList[index].Results;
            }
        }

        private void delRuleBtn_Click(object sender, EventArgs e)
        {
            var selected = dgvRules.SelectedRows;
            for (int i = selected.Count - 1; i >= 0; i--)
            {
                int index = selected[i].Index;
                foreach (var c in ruleList[index].Conditions)
                    c.Variable.UsedCount--;
                foreach (var r in ruleList[index].Results)
                    r.Variable.UsedCount--;
                ruleList.RemoveAt(index);
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvRules.CurrentCell == null)
            {
                conditionsList.DataSource = null;
                resultsList.DataSource = null;
                return;
            }
            var index = dgvRules.CurrentCell.RowIndex;
            conditionsList.DataSource = ruleList[index].Conditions;
            resultsList.DataSource = ruleList[index].Results;
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (!chboxReordering.Checked)
                return;
            if (e.KeyCode != Keys.Down && e.KeyCode != Keys.Up)
                return;
            if (dgvRules.CurrentCell == null)
                return;
            var index = dgvRules.CurrentCell.RowIndex;
            if (e.KeyCode == Keys.Up)
            {
                if (index == 0)
                    return;
                var tmp = ruleList[index - 1];
                ruleList[index - 1] = ruleList[index];
                ruleList[index] = tmp;
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (index == ruleList.Count - 1)
                    return;
                var tmp = ruleList[index + 1];
                ruleList[index + 1] = ruleList[index];
                ruleList[index] = tmp;
            }

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var window = MessageBox.Show(
                "Сохранить перед закрытием?",
                "Сохранить перед закрытием?",
                MessageBoxButtons.YesNoCancel);

            if (window == DialogResult.Yes)
                e.Cancel = !SaveChose();
            else
                e.Cancel = (window == DialogResult.Cancel);
        }

        private void консультацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultForm cf = new ConsultForm(ruleList, variableList, domainList);
            cf.ShowDialog();
        }
    }
}
