﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShell
{
    public partial class EditRuleForm : Form
    {
        Rule rule;
        BindingList<Variable> variablesList;
        BindingList<Domain> domainList;
        BindingList<Fact> conditions;
        BindingList<Fact> results;
        public EditRuleForm(Rule rule, BindingList<Variable> variablesList, BindingList<Domain> domainList)
        {
            InitializeComponent();
            this.rule = rule;
            this.variablesList = variablesList;
            this.domainList = domainList;
            conditions = new BindingList<Fact>();
            results = new BindingList<Fact>();

            ruleNameTxt.Text = rule.Name;
            conditionsList.DataSource = conditions;
            resultsList.DataSource = results;
            txtReason.Text = rule.Reason;

            for (int i = 0; i < rule.Conditions.Count; i++)
                conditions.Add(new Fact(rule.Conditions[i].Variable, rule.Conditions[i].Value));
            for (int i = 0; i < rule.Results.Count; i++)
                results.Add(new Fact(rule.Results[i].Variable, rule.Results[i].Value));

            okBtn.Enabled = OpenOkBtn();
        }

        private void addConditionBtn_Click(object sender, EventArgs e)
        {
            Fact newCondition = new Fact();
            AddConditionForm acf = new AddConditionForm(newCondition, variablesList, domainList, false);
            if (acf.ShowDialog() == DialogResult.OK)
            {
                newCondition.Variable.UsedCount++;
                conditions.Add(newCondition);
            }
            okBtn.Enabled = OpenOkBtn();
        }

        private void editConditionBtn_Click(object sender, EventArgs e)
        {
            var index = conditionsList.SelectedIndex;
            if (index == -1)
                return;
            Fact editedCondition = conditions[index];
            AddConditionForm acf = new AddConditionForm(editedCondition, variablesList, domainList, false);
            if (acf.ShowDialog() == DialogResult.OK)
            {
                conditions[index].Variable.UsedCount--;
                conditions[index] = editedCondition;
                conditions[index].Variable.UsedCount++;
            }
        }

        private void delConditionBtn_Click(object sender, EventArgs e)
        {
            var index = conditionsList.SelectedIndex;
            if (index == -1)
                return;
            conditions[index].Variable.UsedCount--;
            conditions.RemoveAt(index);
            okBtn.Enabled = OpenOkBtn();
        }

        private void addResultBtn_Click(object sender, EventArgs e)
        {
            Fact newResult = new Fact();
            AddConditionForm acf = new AddConditionForm(newResult, variablesList, domainList, true);
            if (acf.ShowDialog() == DialogResult.OK)
            {
                newResult.Variable.UsedCount++;
                results.Add(newResult);
            }
            okBtn.Enabled = OpenOkBtn();
        }

        private void editResultBtn_Click(object sender, EventArgs e)
        {
            var index = resultsList.SelectedIndex;
            if (index == -1)
                return;
            Fact editedResult = results[index];
            AddConditionForm acf = new AddConditionForm(editedResult, variablesList, domainList, true);
            if (acf.ShowDialog() == DialogResult.OK)
            {
                results[index].Variable.UsedCount--;
                results[index] = editedResult;
                results[index].Variable.UsedCount++;
            }
        }

        private void delResultBtn_Click(object sender, EventArgs e)
        {
            var index = resultsList.SelectedIndex;
            if (index == -1)
                return;
            results[index].Variable.UsedCount--;
            results.RemoveAt(index);
            okBtn.Enabled = OpenOkBtn();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            rule.Name = ruleNameTxt.Text;
            rule.Conditions = conditions;
            rule.Results = results;
            rule.Reason = txtReason.Text;
        }

        bool OpenOkBtn()
        {
            var name = !string.IsNullOrEmpty(ruleNameTxt.Text);
            var condi = conditions.Count != 0;
            var res = results.Count != 0;
            var reason = !string.IsNullOrEmpty(txtReason.Text);
            return name && condi && res && reason;
        }

        private void txtReason_TextChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
        }

        private void ruleNameTxt_TextChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenOkBtn();
        }
    }
}
