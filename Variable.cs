﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyShell
{
    public class Variable
    {
        VariableType variableType;
        public string Name { get; set; }
        public Domain Domain { get; set; }
        public VariableType VariableType { 
            get { return variableType; }
            set { 
                variableType = value;
                VariableTypeStr = GetDescription(variableType);
            }
        }
        public string VariableTypeStr { get; set; }
        public string QuestionText { get; set; }

        private int usedCount;
        public int UsedCount { 
            get { 
                return usedCount; 
            }
            set {
                Domain.UsedCount += Math.Abs(UsedCount - value);
                usedCount = value;
            }
        }

        public Variable(string name, Domain domain, VariableType variableType, string questionText)
        {
            Name = name;
            Domain = domain;
            VariableType = variableType;
            VariableTypeStr = GetDescription(variableType);
            QuestionText = questionText;
        }

        public Variable()
        {}

        public string GetDescription(VariableType enumerationValue)
        {
            Type type = enumerationValue.GetType();
            //Tries to find a DescriptionAttribute for a potential friendly name for the enum
            MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0) //Pull out the description value
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            //If we have no description attribute, just return the ToString of the enum
            return enumerationValue.ToString();
        }

        public override string ToString()
        {
            return Name.ToString();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Variable))
                return false;
            Variable v = (Variable)obj;
            return Name.Equals(v.Name) && QuestionText.Equals(v.QuestionText) && Domain.Equals(v.Domain) && VariableType.Equals(v.VariableType);
        }
    }
}
