﻿namespace MyShell
{
    partial class EditRuleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ruleNameTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.conditionsList = new System.Windows.Forms.ListBox();
            this.delConditionBtn = new System.Windows.Forms.Button();
            this.editConditionBtn = new System.Windows.Forms.Button();
            this.addConditionBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.resultsList = new System.Windows.Forms.ListBox();
            this.delResultBtn = new System.Windows.Forms.Button();
            this.addResultBtn = new System.Windows.Forms.Button();
            this.editResultBtn = new System.Windows.Forms.Button();
            this.declineBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Имя правила:";
            // 
            // ruleNameTxt
            // 
            this.ruleNameTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ruleNameTxt.Location = new System.Drawing.Point(11, 26);
            this.ruleNameTxt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ruleNameTxt.Name = "ruleNameTxt";
            this.ruleNameTxt.Size = new System.Drawing.Size(690, 22);
            this.ruleNameTxt.TabIndex = 1;
            this.ruleNameTxt.TextChanged += new System.EventHandler(this.ruleNameTxt_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.conditionsList);
            this.groupBox1.Controls.Add(this.delConditionBtn);
            this.groupBox1.Controls.Add(this.editConditionBtn);
            this.groupBox1.Controls.Add(this.addConditionBtn);
            this.groupBox1.Location = new System.Drawing.Point(11, 58);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(345, 182);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Посылка";
            // 
            // conditionsList
            // 
            this.conditionsList.FormattingEnabled = true;
            this.conditionsList.ItemHeight = 16;
            this.conditionsList.Location = new System.Drawing.Point(5, 19);
            this.conditionsList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.conditionsList.Name = "conditionsList";
            this.conditionsList.Size = new System.Drawing.Size(340, 132);
            this.conditionsList.TabIndex = 16;
            // 
            // delConditionBtn
            // 
            this.delConditionBtn.Location = new System.Drawing.Point(231, 150);
            this.delConditionBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delConditionBtn.Name = "delConditionBtn";
            this.delConditionBtn.Size = new System.Drawing.Size(115, 26);
            this.delConditionBtn.TabIndex = 11;
            this.delConditionBtn.Text = "Удалить";
            this.delConditionBtn.UseVisualStyleBackColor = true;
            this.delConditionBtn.Click += new System.EventHandler(this.delConditionBtn_Click);
            // 
            // editConditionBtn
            // 
            this.editConditionBtn.Location = new System.Drawing.Point(117, 150);
            this.editConditionBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editConditionBtn.Name = "editConditionBtn";
            this.editConditionBtn.Size = new System.Drawing.Size(115, 26);
            this.editConditionBtn.TabIndex = 10;
            this.editConditionBtn.Text = "Изменить";
            this.editConditionBtn.UseVisualStyleBackColor = true;
            this.editConditionBtn.Click += new System.EventHandler(this.editConditionBtn_Click);
            // 
            // addConditionBtn
            // 
            this.addConditionBtn.Location = new System.Drawing.Point(4, 150);
            this.addConditionBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addConditionBtn.Name = "addConditionBtn";
            this.addConditionBtn.Size = new System.Drawing.Size(115, 26);
            this.addConditionBtn.TabIndex = 9;
            this.addConditionBtn.Text = "Добавить";
            this.addConditionBtn.UseVisualStyleBackColor = true;
            this.addConditionBtn.Click += new System.EventHandler(this.addConditionBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.resultsList);
            this.groupBox2.Controls.Add(this.delResultBtn);
            this.groupBox2.Controls.Add(this.addResultBtn);
            this.groupBox2.Controls.Add(this.editResultBtn);
            this.groupBox2.Location = new System.Drawing.Point(361, 58);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(345, 182);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Заключение";
            // 
            // resultsList
            // 
            this.resultsList.FormattingEnabled = true;
            this.resultsList.ItemHeight = 16;
            this.resultsList.Location = new System.Drawing.Point(0, 19);
            this.resultsList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.resultsList.Name = "resultsList";
            this.resultsList.Size = new System.Drawing.Size(344, 132);
            this.resultsList.TabIndex = 15;
            // 
            // delResultBtn
            // 
            this.delResultBtn.Location = new System.Drawing.Point(228, 150);
            this.delResultBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delResultBtn.Name = "delResultBtn";
            this.delResultBtn.Size = new System.Drawing.Size(115, 26);
            this.delResultBtn.TabIndex = 14;
            this.delResultBtn.Text = "Удалить";
            this.delResultBtn.UseVisualStyleBackColor = true;
            this.delResultBtn.Click += new System.EventHandler(this.delResultBtn_Click);
            // 
            // addResultBtn
            // 
            this.addResultBtn.Location = new System.Drawing.Point(1, 150);
            this.addResultBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addResultBtn.Name = "addResultBtn";
            this.addResultBtn.Size = new System.Drawing.Size(115, 26);
            this.addResultBtn.TabIndex = 12;
            this.addResultBtn.Text = "Добавить";
            this.addResultBtn.UseVisualStyleBackColor = true;
            this.addResultBtn.Click += new System.EventHandler(this.addResultBtn_Click);
            // 
            // editResultBtn
            // 
            this.editResultBtn.Location = new System.Drawing.Point(115, 150);
            this.editResultBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editResultBtn.Name = "editResultBtn";
            this.editResultBtn.Size = new System.Drawing.Size(115, 26);
            this.editResultBtn.TabIndex = 13;
            this.editResultBtn.Text = "Изменить";
            this.editResultBtn.UseVisualStyleBackColor = true;
            this.editResultBtn.Click += new System.EventHandler(this.editResultBtn_Click);
            // 
            // declineBtn
            // 
            this.declineBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.declineBtn.Location = new System.Drawing.Point(627, 331);
            this.declineBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.declineBtn.Name = "declineBtn";
            this.declineBtn.Size = new System.Drawing.Size(78, 26);
            this.declineBtn.TabIndex = 4;
            this.declineBtn.Text = "Отмена";
            this.declineBtn.UseVisualStyleBackColor = true;
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Location = new System.Drawing.Point(543, 331);
            this.okBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(78, 26);
            this.okBtn.TabIndex = 5;
            this.okBtn.Text = "ОК";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(14, 327);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(686, 2);
            this.label2.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Пояснение:";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtReason.Location = new System.Drawing.Point(11, 261);
            this.txtReason.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(690, 54);
            this.txtReason.TabIndex = 8;
            this.txtReason.TextChanged += new System.EventHandler(this.txtReason_TextChanged);
            // 
            // EditRuleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 360);
            this.Controls.Add(this.txtReason);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.declineBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ruleNameTxt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditRuleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование правила";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ruleNameTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button declineBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button delConditionBtn;
        private System.Windows.Forms.Button editConditionBtn;
        private System.Windows.Forms.Button addConditionBtn;
        private System.Windows.Forms.Button delResultBtn;
        private System.Windows.Forms.Button addResultBtn;
        private System.Windows.Forms.Button editResultBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.ListBox conditionsList;
        private System.Windows.Forms.ListBox resultsList;
    }
}