﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShell
{
    public partial class ConsultForm : Form
    {
        ConsultBubble bubbleOld;
        BindingList<Rule> rulesList;
        BindingList<Variable> variableList;
        BindingList<Domain> domainList;
        WorkingMemory memory;

        public ConsultForm(BindingList<Rule> rulesList, BindingList<Variable> variableList, BindingList<Domain> domainList)
        {
            InitializeComponent();
            this.rulesList = rulesList;
            this.variableList = variableList;
            this.domainList = domainList;

            panel1.AutoScroll = true;

            memory = new WorkingMemory();
            memory.UntriggeredRules = rulesList.AsEnumerable().ToList();

            bubbleOld = new ConsultBubble();
            AddMessage("Выберите цель консультации:", MsgType.In);
            foreach (var goal in variableList)
                if (goal.VariableType == VariableType.Concluded)
                    consultGoalCbx.Items.Add(goal);
        }

        void AddMessage(string message, MsgType msgType)
        {
            ConsultBubble bubble = new ConsultBubble(message, msgType);
            bubble.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
            bubble.Size = consultBubble1.Size;
            if (msgType == MsgType.In)
                bubble.Location = consultBubble1.Location;
            else
                bubble.Location = consultBubble2.Location;
            bubble.Top = bubbleOld.Bottom + 20;
            panel1.Controls.Add(bubble);

            bubbleOld = bubble;
        }

        private void panel1_ControlAdded(object sender, ControlEventArgs e)
        {
            panel1.ScrollControlIntoView(e.Control);
        }

        private ManualResetEvent valueGetEvent = new ManualResetEvent(false);
        DomainValue currentValue;
        private async void answerBtn_Click(object sender, EventArgs e)
        {
            if (memory.GoalVariable == null && consultGoalCbx.SelectedIndex != -1 && consultGoalCbx.SelectedItem is Variable)
            {
                memory.GoalVariable = (Variable)consultGoalCbx.SelectedItem;
                AddMessage(memory.GoalVariable.Name, MsgType.Out);
                Task.Run(() => ConcludeGoalVariable(memory.GoalVariable));
            }
            else
            {
                currentValue = (DomainValue)consultGoalCbx.SelectedItem;
                AddMessage(currentValue.Value, MsgType.Out);
                valueGetEvent.Set();
            }
        }

        public void ConcludeGoalVariable(Variable variable)
        {
            memory.GoalVariable = variable;
            var domainValue = Conclude(variable);
            CheckGoalInitialized(domainValue);
        }

        void CheckGoalInitialized(DomainValue domainValue)
        {
            BeginInvoke(new Action(() =>
            {
                if (domainValue == null)
                    this.AddMessage("Цель консультации не была достигнута.\nОбратитесь к другой ЭС.", MsgType.In);
                else
                {
                    this.AddMessage("Цель консультации достигнута!\nРезультат:" + string.Format("\n{0} - {1}",
                            memory.GoalVariable.Name, domainValue.Value), MsgType.In);
                }
                consultGoalCbx.Visible = false;
                answerBtn.Visible = false;
                linkLabel1.Visible = true;
                panel1.ScrollControlIntoView(panel1.Controls[Controls.Count - 1]);
            }));
        }

        DomainValue AskMessage(Variable variable)
        {
            BeginInvoke(new Action(() =>
            {
                consultGoalCbx.Items.Clear();
                foreach (var d in variable.Domain.Values)
                    consultGoalCbx.Items.Add(d);
                AddMessage(variable.QuestionText, MsgType.In);
            }));
            valueGetEvent.WaitOne();
            valueGetEvent.Reset();
            return currentValue;
        }
        private DomainValue Conclude(Variable variable)
        {
            var fact = memory.TrueFacts.FirstOrDefault(x => x.Variable.Equals(variable));

            if (fact != null)
                return fact.Value;

            if (variable.VariableType == VariableType.Asked)
            {
                var domainValue = AskMessage(variable);
                memory.TrueFacts.Add(new Fact(variable, domainValue));
                return domainValue;
            }

            foreach (var rule in memory.UntriggeredRules.Where(r => r.Results.Any(c => c.Variable.Equals(variable))).ToList())
            {
                var source = TryApplyRule(rule);
                if (source != null)
                {
                    memory.TrueFacts.AddRange(source);
                    return source.First(x => x.Variable.Equals(variable)).Value;
                }
            }

            if (variable.VariableType == VariableType.Conclude_Asked)
            {
                var domainValue = AskMessage(variable);
                memory.TrueFacts.Add(new Fact(variable, domainValue));
                return domainValue;
            }

            return null;
        }

        private List<Fact> TryApplyRule(Rule rule)
        {
            memory.UntriggeredRules.Remove(rule);
            foreach (var fact in rule.Conditions)
            {
                var fact2 = memory.TrueFacts.FirstOrDefault(x => x.Variable.Equals(fact.Variable));

                var domainValue = fact2 != null ? fact2.Value : Conclude(fact.Variable);
                if (domainValue == null || !fact.Value.Value.Equals(domainValue.Value))
                    return null;
                
            }

            foreach (var fact in rule.Results)
                memory.InferenceDictionary[fact.Variable] = rule;

            return rule.Results.ToList();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ExplanationForm ef = new ExplanationForm(memory);
            ef.ShowDialog();
        }
    }
}
