﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShell
{
    public class Fact
    {
        public Variable Variable { get; set; }
        public DomainValue Value { get; set; }
        public Fact(Variable variable, DomainValue domainValue)
        {
            Variable = variable;
            Value = domainValue;
        }

        public Fact() { }

        public override string ToString()
        {
            return Variable + " = " + Value.Value;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Fact))
                return false;
            Fact f = (Fact)obj;
            return Variable.Equals(f.Variable) && Value.Value.Equals(f.Value.Value);
        }
    }
}
