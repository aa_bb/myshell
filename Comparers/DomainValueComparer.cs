﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShell.Comparers
{
    public class DomainValueComparer : IEqualityComparer<DomainValue>
    {
        public int Compare(DomainValue x, DomainValue y)
        {
            return x.Value.CompareTo(y.Value);
        }

        public bool Equals(DomainValue x, DomainValue y)
        {
            return x.Value.Equals(y.Value);
        }

        public int GetHashCode(DomainValue obj)
        {
            throw new NotImplementedException();
        }
    }
}
