﻿namespace MyShell
{
    partial class AddConditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.conditionLeftCbx = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            this.declineBtn = new System.Windows.Forms.Button();
            this.conditionRightCbx = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // conditionLeftCbx
            // 
            this.conditionLeftCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.conditionLeftCbx.FormattingEnabled = true;
            this.conditionLeftCbx.Location = new System.Drawing.Point(11, 12);
            this.conditionLeftCbx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.conditionLeftCbx.Name = "conditionLeftCbx";
            this.conditionLeftCbx.Size = new System.Drawing.Size(655, 24);
            this.conditionLeftCbx.TabIndex = 0;
            this.conditionLeftCbx.SelectedIndexChanged += new System.EventHandler(this.conditionLeftCbx_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(670, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 27);
            this.button1.TabIndex = 1;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(349, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "=";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(11, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(686, 2);
            this.label2.TabIndex = 7;
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Location = new System.Drawing.Point(537, 113);
            this.okBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(78, 26);
            this.okBtn.TabIndex = 9;
            this.okBtn.Text = "ОК";
            this.okBtn.UseVisualStyleBackColor = true;
            // 
            // declineBtn
            // 
            this.declineBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.declineBtn.Location = new System.Drawing.Point(622, 113);
            this.declineBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.declineBtn.Name = "declineBtn";
            this.declineBtn.Size = new System.Drawing.Size(78, 26);
            this.declineBtn.TabIndex = 8;
            this.declineBtn.Text = "Отмена";
            this.declineBtn.UseVisualStyleBackColor = true;
            // 
            // conditionRightCbx
            // 
            this.conditionRightCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.conditionRightCbx.FormattingEnabled = true;
            this.conditionRightCbx.Location = new System.Drawing.Point(11, 75);
            this.conditionRightCbx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.conditionRightCbx.Name = "conditionRightCbx";
            this.conditionRightCbx.Size = new System.Drawing.Size(690, 24);
            this.conditionRightCbx.TabIndex = 10;
            this.conditionRightCbx.SelectedIndexChanged += new System.EventHandler(this.conditionRightCbx_SelectedIndexChanged);
            // 
            // AddConditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(711, 142);
            this.Controls.Add(this.conditionRightCbx);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.declineBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.conditionLeftCbx);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddConditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление факта посылки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox conditionLeftCbx;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button declineBtn;
        private System.Windows.Forms.ComboBox conditionRightCbx;
    }
}