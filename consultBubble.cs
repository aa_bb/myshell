﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShell
{
    public partial class ConsultBubble : UserControl
    {
        public ConsultBubble()
        {
            InitializeComponent();
            SetHeight();
        }
        public ConsultBubble(string message, MsgType msgType)
        {
            InitializeComponent();
            bubbleLabel.Text = message;
            if (msgType == MsgType.In)
            {
                BackColor = Color.CornflowerBlue;
                bubbleLabel.TextAlign = ContentAlignment.MiddleLeft;
            }
            else
            {
                BackColor = Color.Silver;
                bubbleLabel.TextAlign = ContentAlignment.MiddleRight;
            }
            SetHeight();
        }

        void SetHeight()
        {
            Graphics g = CreateGraphics();
            SizeF size = g.MeasureString(bubbleLabel.Text, bubbleLabel.Font, bubbleLabel.Width);

            bubbleLabel.Height = (int) size.Height + 2;
            Height = bubbleLabel.Bottom + 20;
        }

        private void ConsultBubble_Resize(object sender, EventArgs e)
        {
            SetHeight();
        }
    }

    public enum MsgType
    {
        In,
        Out
    }
}
