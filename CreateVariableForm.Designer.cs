﻿namespace MyShell
{
    partial class CreateVariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.varNameTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddDomain = new System.Windows.Forms.Button();
            this.domainCbx = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioAskedConcluded = new System.Windows.Forms.RadioButton();
            this.radioConcluded = new System.Windows.Forms.RadioButton();
            this.radioAsked = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            this.declineBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // varNameTxt
            // 
            this.varNameTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.varNameTxt.Location = new System.Drawing.Point(11, 32);
            this.varNameTxt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.varNameTxt.Name = "varNameTxt";
            this.varNameTxt.Size = new System.Drawing.Size(690, 22);
            this.varNameTxt.TabIndex = 3;
            this.varNameTxt.TextChanged += new System.EventHandler(this.varNameTxt_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Имя переменной:";
            // 
            // btnAddDomain
            // 
            this.btnAddDomain.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAddDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddDomain.Location = new System.Drawing.Point(670, 81);
            this.btnAddDomain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddDomain.Name = "btnAddDomain";
            this.btnAddDomain.Size = new System.Drawing.Size(30, 27);
            this.btnAddDomain.TabIndex = 5;
            this.btnAddDomain.Text = "+";
            this.btnAddDomain.UseVisualStyleBackColor = true;
            this.btnAddDomain.Click += new System.EventHandler(this.btnAddDomain_Click);
            // 
            // domainCbx
            // 
            this.domainCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.domainCbx.FormattingEnabled = true;
            this.domainCbx.Location = new System.Drawing.Point(11, 83);
            this.domainCbx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.domainCbx.Name = "domainCbx";
            this.domainCbx.Size = new System.Drawing.Size(655, 24);
            this.domainCbx.TabIndex = 4;
            this.domainCbx.SelectedIndexChanged += new System.EventHandler(this.domainCbx_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Домен:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioAskedConcluded);
            this.groupBox1.Controls.Add(this.radioConcluded);
            this.groupBox1.Controls.Add(this.radioAsked);
            this.groupBox1.Location = new System.Drawing.Point(11, 118);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(690, 110);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тип переменной";
            // 
            // radioAskedConcluded
            // 
            this.radioAskedConcluded.AutoSize = true;
            this.radioAskedConcluded.Location = new System.Drawing.Point(13, 78);
            this.radioAskedConcluded.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioAskedConcluded.Name = "radioAskedConcluded";
            this.radioAskedConcluded.Size = new System.Drawing.Size(207, 21);
            this.radioAskedConcluded.TabIndex = 2;
            this.radioAskedConcluded.TabStop = true;
            this.radioAskedConcluded.Text = "Выводимо-запрашиваемая";
            this.radioAskedConcluded.UseVisualStyleBackColor = true;
            this.radioAskedConcluded.CheckedChanged += new System.EventHandler(this.radioAskedConcluded_CheckedChanged);
            // 
            // radioConcluded
            // 
            this.radioConcluded.AutoSize = true;
            this.radioConcluded.Location = new System.Drawing.Point(13, 52);
            this.radioConcluded.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioConcluded.Name = "radioConcluded";
            this.radioConcluded.Size = new System.Drawing.Size(104, 21);
            this.radioConcluded.TabIndex = 1;
            this.radioConcluded.TabStop = true;
            this.radioConcluded.Text = "Выводимая";
            this.radioConcluded.UseVisualStyleBackColor = true;
            this.radioConcluded.CheckedChanged += new System.EventHandler(this.radioConcluded_CheckedChanged);
            // 
            // radioAsked
            // 
            this.radioAsked.AutoSize = true;
            this.radioAsked.Location = new System.Drawing.Point(13, 26);
            this.radioAsked.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioAsked.Name = "radioAsked";
            this.radioAsked.Size = new System.Drawing.Size(137, 21);
            this.radioAsked.TabIndex = 0;
            this.radioAsked.TabStop = true;
            this.radioAsked.Text = "Запрашиваемая";
            this.radioAsked.UseVisualStyleBackColor = true;
            this.radioAsked.CheckedChanged += new System.EventHandler(this.radioAsked_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 239);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Текст вопроса:";
            // 
            // txtQuestion
            // 
            this.txtQuestion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtQuestion.Location = new System.Drawing.Point(11, 261);
            this.txtQuestion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQuestion.Multiline = true;
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.Size = new System.Drawing.Size(690, 54);
            this.txtQuestion.TabIndex = 9;
            this.txtQuestion.TextChanged += new System.EventHandler(this.txtQuestion_TextChanged);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(14, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(686, 2);
            this.label4.TabIndex = 10;
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Location = new System.Drawing.Point(537, 332);
            this.okBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(78, 26);
            this.okBtn.TabIndex = 12;
            this.okBtn.Text = "ОК";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // declineBtn
            // 
            this.declineBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.declineBtn.Location = new System.Drawing.Point(622, 332);
            this.declineBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.declineBtn.Name = "declineBtn";
            this.declineBtn.Size = new System.Drawing.Size(78, 26);
            this.declineBtn.TabIndex = 11;
            this.declineBtn.Text = "Отмена";
            this.declineBtn.UseVisualStyleBackColor = true;
            // 
            // CreateVariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 367);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.declineBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtQuestion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAddDomain);
            this.Controls.Add(this.domainCbx);
            this.Controls.Add(this.varNameTxt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateVariableForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Создание переменной";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox varNameTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddDomain;
        private System.Windows.Forms.ComboBox domainCbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioAskedConcluded;
        private System.Windows.Forms.RadioButton radioConcluded;
        private System.Windows.Forms.RadioButton radioAsked;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button declineBtn;
    }
}