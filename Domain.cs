﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShell
{
    public class Domain
    {
        public string Name { get; set; }
        public BindingList<DomainValue> Values { get; set; }
        public int UsedCount { get; set; }

        public Domain(string name, BindingList<DomainValue> values)
        {
            Name = name;
            Values = values;
        }

        public Domain()
        {
            Values = new BindingList<DomainValue>();
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Domain))
                return false;
            Domain d = (Domain)obj;
            bool res = Name.Equals(d.Name) && Values.Count == d.Values.Count;
            if (res)
                for (int i = 0; res && i < Values.Count; i++)
                    if (!Values[i].Value.Equals(d.Values[i].Value))
                        res = false;
            return res;
        }
    }
}
