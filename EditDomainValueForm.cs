﻿using System;
using System.Windows.Forms;

namespace MyShell
{
    public partial class EditDomainValueForm : Form
    {
        DomainValue domainValue;
        public EditDomainValueForm(DomainValue domainValue)
        {
            InitializeComponent();
            okBtn.DialogResult = DialogResult.OK;
            okBtn.Enabled = OpenBtnOk();
            closeBtn.DialogResult = DialogResult.Cancel;

            this.domainValue = domainValue;
            domainValueTxt.Text = domainValue.Value;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            domainValue.Value = domainValueTxt.Text;
        }

        private void domainValueTxt_TextChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = OpenBtnOk();
        }

        bool OpenBtnOk()
        {
            return !string.IsNullOrEmpty(domainValueTxt.Text);
        }
    }
}
