﻿namespace MyShell
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьКакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.консультацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.rulesTab = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.resultsList = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.conditionsList = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chboxReordering = new System.Windows.Forms.CheckBox();
            this.delRuleBtn = new System.Windows.Forms.Button();
            this.editRuleBtn = new System.Windows.Forms.Button();
            this.addRuleBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvRules = new System.Windows.Forms.DataGridView();
            this.varsTab = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.delVarBtn = new System.Windows.Forms.Button();
            this.editVarBtn = new System.Windows.Forms.Button();
            this.addVarBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvVars = new System.Windows.Forms.DataGridView();
            this.domainTab = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.domainValueList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.delDomainBtn = new System.Windows.Forms.Button();
            this.editDomainBtn = new System.Windows.Forms.Button();
            this.addDomainBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvDomain = new System.Windows.Forms.DataGridView();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.rulesTab.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRules)).BeginInit();
            this.varsTab.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVars)).BeginInit();
            this.domainTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomain)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.консультацияToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(893, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьФайлToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.сохранитьКакToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(59, 24);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьФайлToolStripMenuItem
            // 
            this.открытьФайлToolStripMenuItem.Name = "открытьФайлToolStripMenuItem";
            this.открытьФайлToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl + O";
            this.открытьФайлToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.открытьФайлToolStripMenuItem.Size = new System.Drawing.Size(280, 26);
            this.открытьФайлToolStripMenuItem.Text = "Открыть файл...";
            this.открытьФайлToolStripMenuItem.Click += new System.EventHandler(this.открытьФайлToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl + S";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(280, 26);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // сохранитьКакToolStripMenuItem
            // 
            this.сохранитьКакToolStripMenuItem.Name = "сохранитьКакToolStripMenuItem";
            this.сохранитьКакToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.сохранитьКакToolStripMenuItem.Size = new System.Drawing.Size(280, 26);
            this.сохранитьКакToolStripMenuItem.Text = "Сохранить как...";
            this.сохранитьКакToolStripMenuItem.Click += new System.EventHandler(this.сохранитьКакToolStripMenuItem_Click);
            // 
            // консультацияToolStripMenuItem
            // 
            this.консультацияToolStripMenuItem.Name = "консультацияToolStripMenuItem";
            this.консультацияToolStripMenuItem.Size = new System.Drawing.Size(120, 24);
            this.консультацияToolStripMenuItem.Text = "Консультация";
            this.консультацияToolStripMenuItem.Click += new System.EventHandler(this.консультацияToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.rulesTab);
            this.tabControl1.Controls.Add(this.varsTab);
            this.tabControl1.Controls.Add(this.domainTab);
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(893, 456);
            this.tabControl1.TabIndex = 1;
            // 
            // rulesTab
            // 
            this.rulesTab.Controls.Add(this.groupBox5);
            this.rulesTab.Controls.Add(this.groupBox4);
            this.rulesTab.Controls.Add(this.groupBox3);
            this.rulesTab.Controls.Add(this.panel2);
            this.rulesTab.Location = new System.Drawing.Point(4, 25);
            this.rulesTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rulesTab.Name = "rulesTab";
            this.rulesTab.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rulesTab.Size = new System.Drawing.Size(885, 427);
            this.rulesTab.TabIndex = 0;
            this.rulesTab.Text = "Правила";
            this.rulesTab.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.resultsList);
            this.groupBox5.Location = new System.Drawing.Point(587, 293);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(292, 129);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Заключение";
            // 
            // resultsList
            // 
            this.resultsList.FormattingEnabled = true;
            this.resultsList.ItemHeight = 16;
            this.resultsList.Location = new System.Drawing.Point(5, 17);
            this.resultsList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.resultsList.Name = "resultsList";
            this.resultsList.Size = new System.Drawing.Size(282, 132);
            this.resultsList.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.conditionsList);
            this.groupBox4.Location = new System.Drawing.Point(587, 151);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(292, 138);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Посылка";
            // 
            // conditionsList
            // 
            this.conditionsList.FormattingEnabled = true;
            this.conditionsList.ItemHeight = 16;
            this.conditionsList.Location = new System.Drawing.Point(5, 17);
            this.conditionsList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.conditionsList.Name = "conditionsList";
            this.conditionsList.Size = new System.Drawing.Size(282, 116);
            this.conditionsList.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chboxReordering);
            this.groupBox3.Controls.Add(this.delRuleBtn);
            this.groupBox3.Controls.Add(this.editRuleBtn);
            this.groupBox3.Controls.Add(this.addRuleBtn);
            this.groupBox3.Location = new System.Drawing.Point(587, 2);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(292, 145);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Редактирование";
            // 
            // chboxReordering
            // 
            this.chboxReordering.AutoSize = true;
            this.chboxReordering.Location = new System.Drawing.Point(9, 116);
            this.chboxReordering.Name = "chboxReordering";
            this.chboxReordering.Size = new System.Drawing.Size(169, 21);
            this.chboxReordering.TabIndex = 3;
            this.chboxReordering.Text = "Режим перемещения";
            this.chboxReordering.UseVisualStyleBackColor = true;
            // 
            // delRuleBtn
            // 
            this.delRuleBtn.Location = new System.Drawing.Point(5, 84);
            this.delRuleBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delRuleBtn.Name = "delRuleBtn";
            this.delRuleBtn.Size = new System.Drawing.Size(282, 27);
            this.delRuleBtn.TabIndex = 2;
            this.delRuleBtn.Text = "Удалить";
            this.delRuleBtn.UseVisualStyleBackColor = true;
            this.delRuleBtn.Click += new System.EventHandler(this.delRuleBtn_Click);
            // 
            // editRuleBtn
            // 
            this.editRuleBtn.Location = new System.Drawing.Point(5, 52);
            this.editRuleBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editRuleBtn.Name = "editRuleBtn";
            this.editRuleBtn.Size = new System.Drawing.Size(282, 27);
            this.editRuleBtn.TabIndex = 1;
            this.editRuleBtn.Text = "Изменить";
            this.editRuleBtn.UseVisualStyleBackColor = true;
            this.editRuleBtn.Click += new System.EventHandler(this.editRuleBtn_Click);
            // 
            // addRuleBtn
            // 
            this.addRuleBtn.Location = new System.Drawing.Point(5, 20);
            this.addRuleBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addRuleBtn.Name = "addRuleBtn";
            this.addRuleBtn.Size = new System.Drawing.Size(282, 27);
            this.addRuleBtn.TabIndex = 0;
            this.addRuleBtn.Text = "Добавить";
            this.addRuleBtn.UseVisualStyleBackColor = true;
            this.addRuleBtn.Click += new System.EventHandler(this.addRuleBtn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvRules);
            this.panel2.Location = new System.Drawing.Point(3, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(579, 422);
            this.panel2.TabIndex = 0;
            // 
            // dgvRules
            // 
            this.dgvRules.AllowDrop = true;
            this.dgvRules.AllowUserToAddRows = false;
            this.dgvRules.AllowUserToDeleteRows = false;
            this.dgvRules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRules.Location = new System.Drawing.Point(0, 0);
            this.dgvRules.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvRules.Name = "dgvRules";
            this.dgvRules.ReadOnly = true;
            this.dgvRules.RowHeadersWidth = 51;
            this.dgvRules.RowTemplate.Height = 28;
            this.dgvRules.Size = new System.Drawing.Size(579, 422);
            this.dgvRules.TabIndex = 0;
            this.dgvRules.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            this.dgvRules.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // varsTab
            // 
            this.varsTab.Controls.Add(this.groupBox8);
            this.varsTab.Controls.Add(this.groupBox7);
            this.varsTab.Controls.Add(this.groupBox6);
            this.varsTab.Controls.Add(this.panel3);
            this.varsTab.Location = new System.Drawing.Point(4, 25);
            this.varsTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.varsTab.Name = "varsTab";
            this.varsTab.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.varsTab.Size = new System.Drawing.Size(885, 427);
            this.varsTab.TabIndex = 1;
            this.varsTab.Text = "Переменные";
            this.varsTab.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtQuestion);
            this.groupBox7.Location = new System.Drawing.Point(580, 141);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(299, 126);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Текст вопроса";
            // 
            // txtQuestion
            // 
            this.txtQuestion.Location = new System.Drawing.Point(5, 20);
            this.txtQuestion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQuestion.Multiline = true;
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.Size = new System.Drawing.Size(288, 98);
            this.txtQuestion.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.delVarBtn);
            this.groupBox6.Controls.Add(this.editVarBtn);
            this.groupBox6.Controls.Add(this.addVarBtn);
            this.groupBox6.Location = new System.Drawing.Point(580, 5);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(303, 131);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Редактирование";
            // 
            // delVarBtn
            // 
            this.delVarBtn.Location = new System.Drawing.Point(8, 92);
            this.delVarBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delVarBtn.Name = "delVarBtn";
            this.delVarBtn.Size = new System.Drawing.Size(292, 31);
            this.delVarBtn.TabIndex = 2;
            this.delVarBtn.Text = "Удалить";
            this.delVarBtn.UseVisualStyleBackColor = true;
            this.delVarBtn.Click += new System.EventHandler(this.delVarBtn_Click);
            // 
            // editVarBtn
            // 
            this.editVarBtn.Location = new System.Drawing.Point(8, 56);
            this.editVarBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editVarBtn.Name = "editVarBtn";
            this.editVarBtn.Size = new System.Drawing.Size(292, 31);
            this.editVarBtn.TabIndex = 1;
            this.editVarBtn.Text = "Изменить";
            this.editVarBtn.UseVisualStyleBackColor = true;
            this.editVarBtn.Click += new System.EventHandler(this.editVarBtn_Click);
            // 
            // addVarBtn
            // 
            this.addVarBtn.Location = new System.Drawing.Point(8, 20);
            this.addVarBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addVarBtn.Name = "addVarBtn";
            this.addVarBtn.Size = new System.Drawing.Size(292, 31);
            this.addVarBtn.TabIndex = 0;
            this.addVarBtn.Text = "Добавить";
            this.addVarBtn.UseVisualStyleBackColor = true;
            this.addVarBtn.Click += new System.EventHandler(this.addVarBtn_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvVars);
            this.panel3.Location = new System.Drawing.Point(3, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(572, 427);
            this.panel3.TabIndex = 0;
            // 
            // dgvVars
            // 
            this.dgvVars.AllowUserToAddRows = false;
            this.dgvVars.AllowUserToDeleteRows = false;
            this.dgvVars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVars.Location = new System.Drawing.Point(0, 0);
            this.dgvVars.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvVars.Name = "dgvVars";
            this.dgvVars.ReadOnly = true;
            this.dgvVars.RowHeadersWidth = 51;
            this.dgvVars.RowTemplate.Height = 28;
            this.dgvVars.Size = new System.Drawing.Size(572, 427);
            this.dgvVars.TabIndex = 0;
            this.dgvVars.SelectionChanged += new System.EventHandler(this.dataGridView3_SelectionChanged);
            // 
            // domainTab
            // 
            this.domainTab.Controls.Add(this.groupBox2);
            this.domainTab.Controls.Add(this.groupBox1);
            this.domainTab.Controls.Add(this.panel1);
            this.domainTab.Location = new System.Drawing.Point(4, 25);
            this.domainTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.domainTab.Name = "domainTab";
            this.domainTab.Size = new System.Drawing.Size(885, 427);
            this.domainTab.TabIndex = 2;
            this.domainTab.Text = "Домены";
            this.domainTab.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.domainValueList);
            this.groupBox2.Location = new System.Drawing.Point(551, 121);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(332, 306);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Значения домена";
            // 
            // domainValueList
            // 
            this.domainValueList.FormattingEnabled = true;
            this.domainValueList.ItemHeight = 16;
            this.domainValueList.Location = new System.Drawing.Point(6, 21);
            this.domainValueList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.domainValueList.Name = "domainValueList";
            this.domainValueList.Size = new System.Drawing.Size(322, 276);
            this.domainValueList.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.delDomainBtn);
            this.groupBox1.Controls.Add(this.editDomainBtn);
            this.groupBox1.Controls.Add(this.addDomainBtn);
            this.groupBox1.Location = new System.Drawing.Point(551, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(332, 114);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Редактирование";
            // 
            // delDomainBtn
            // 
            this.delDomainBtn.Location = new System.Drawing.Point(6, 79);
            this.delDomainBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.delDomainBtn.Name = "delDomainBtn";
            this.delDomainBtn.Size = new System.Drawing.Size(322, 26);
            this.delDomainBtn.TabIndex = 2;
            this.delDomainBtn.Text = "Удалить";
            this.delDomainBtn.UseVisualStyleBackColor = true;
            this.delDomainBtn.Click += new System.EventHandler(this.delDomainBtn_Click);
            // 
            // editDomainBtn
            // 
            this.editDomainBtn.Location = new System.Drawing.Point(5, 49);
            this.editDomainBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editDomainBtn.Name = "editDomainBtn";
            this.editDomainBtn.Size = new System.Drawing.Size(322, 26);
            this.editDomainBtn.TabIndex = 1;
            this.editDomainBtn.Text = "Изменить";
            this.editDomainBtn.UseVisualStyleBackColor = true;
            this.editDomainBtn.Click += new System.EventHandler(this.editDomainBtn_Click);
            // 
            // addDomainBtn
            // 
            this.addDomainBtn.Location = new System.Drawing.Point(5, 20);
            this.addDomainBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addDomainBtn.Name = "addDomainBtn";
            this.addDomainBtn.Size = new System.Drawing.Size(322, 26);
            this.addDomainBtn.TabIndex = 0;
            this.addDomainBtn.Text = "Добавить";
            this.addDomainBtn.UseVisualStyleBackColor = true;
            this.addDomainBtn.Click += new System.EventHandler(this.addDomainBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvDomain);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(546, 433);
            this.panel1.TabIndex = 1;
            // 
            // dgvDomain
            // 
            this.dgvDomain.AllowUserToAddRows = false;
            this.dgvDomain.AllowUserToDeleteRows = false;
            this.dgvDomain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDomain.Location = new System.Drawing.Point(0, 0);
            this.dgvDomain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvDomain.Name = "dgvDomain";
            this.dgvDomain.ReadOnly = true;
            this.dgvDomain.RowHeadersWidth = 51;
            this.dgvDomain.RowTemplate.Height = 28;
            this.dgvDomain.Size = new System.Drawing.Size(546, 433);
            this.dgvDomain.TabIndex = 0;
            this.dgvDomain.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.listBox1);
            this.groupBox8.Location = new System.Drawing.Point(583, 271);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Size = new System.Drawing.Size(299, 150);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Значения домена";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(9, 20);
            this.listBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(281, 116);
            this.listBox1.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 482);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оболочка ЭС MyShell";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.rulesTab.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRules)).EndInit();
            this.varsTab.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVars)).EndInit();
            this.domainTab.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomain)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem консультацияToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage rulesTab;
        private System.Windows.Forms.TabPage varsTab;
        private System.Windows.Forms.TabPage domainTab;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button delRuleBtn;
        private System.Windows.Forms.Button editRuleBtn;
        private System.Windows.Forms.Button addRuleBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox domainValueList;
        private System.Windows.Forms.Button delDomainBtn;
        private System.Windows.Forms.Button editDomainBtn;
        private System.Windows.Forms.Button addDomainBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListBox resultsList;
        private System.Windows.Forms.ListBox conditionsList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgvVars;
        private System.Windows.Forms.DataGridView dgvDomain;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button delVarBtn;
        private System.Windows.Forms.Button editVarBtn;
        private System.Windows.Forms.Button addVarBtn;
        private System.Windows.Forms.ToolStripMenuItem открытьФайлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьКакToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgvRules;
        private System.Windows.Forms.CheckBox chboxReordering;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListBox listBox1;
    }
}

