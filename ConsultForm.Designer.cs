﻿namespace MyShell
{
    partial class ConsultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.answerBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.consultBubble2 = new MyShell.ConsultBubble();
            this.consultBubble1 = new MyShell.ConsultBubble();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.consultGoalCbx = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // answerBtn
            // 
            this.answerBtn.Location = new System.Drawing.Point(342, 490);
            this.answerBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.answerBtn.Name = "answerBtn";
            this.answerBtn.Size = new System.Drawing.Size(110, 31);
            this.answerBtn.TabIndex = 12;
            this.answerBtn.Text = "Ответить";
            this.answerBtn.UseVisualStyleBackColor = true;
            this.answerBtn.Click += new System.EventHandler(this.answerBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.consultBubble2);
            this.panel1.Controls.Add(this.consultBubble1);
            this.panel1.Location = new System.Drawing.Point(8, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(444, 478);
            this.panel1.TabIndex = 14;
            this.panel1.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.panel1_ControlAdded);
            // 
            // consultBubble2
            // 
            this.consultBubble2.AutoSize = true;
            this.consultBubble2.BackColor = System.Drawing.SystemColors.Control;
            this.consultBubble2.Location = new System.Drawing.Point(162, 57);
            this.consultBubble2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.consultBubble2.Name = "consultBubble2";
            this.consultBubble2.Size = new System.Drawing.Size(269, 48);
            this.consultBubble2.TabIndex = 1;
            this.consultBubble2.Visible = false;
            // 
            // consultBubble1
            // 
            this.consultBubble1.AutoSize = true;
            this.consultBubble1.BackColor = System.Drawing.SystemColors.Control;
            this.consultBubble1.Location = new System.Drawing.Point(4, 5);
            this.consultBubble1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.consultBubble1.Name = "consultBubble1";
            this.consultBubble1.Size = new System.Drawing.Size(269, 48);
            this.consultBubble1.TabIndex = 0;
            this.consultBubble1.Visible = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel1.Location = new System.Drawing.Point(52, 492);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(207, 24);
            this.linkLabel1.TabIndex = 15;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Показать объяснение";
            this.linkLabel1.Visible = false;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // consultGoalCbx
            // 
            this.consultGoalCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.consultGoalCbx.FormattingEnabled = true;
            this.consultGoalCbx.Location = new System.Drawing.Point(6, 494);
            this.consultGoalCbx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.consultGoalCbx.Name = "consultGoalCbx";
            this.consultGoalCbx.Size = new System.Drawing.Size(330, 24);
            this.consultGoalCbx.TabIndex = 16;
            // 
            // ConsultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 526);
            this.Controls.Add(this.consultGoalCbx);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.answerBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConsultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Консультация";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button answerBtn;
        private System.Windows.Forms.Panel panel1;
        private ConsultBubble consultBubble2;
        private ConsultBubble consultBubble1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ComboBox consultGoalCbx;
    }
}